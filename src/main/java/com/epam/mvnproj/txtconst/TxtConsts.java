package com.epam.mvnproj.txtconst;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TxtConsts {

  public static final Logger appLog = LogManager.getLogger(TxtConsts.class);

  public static final String CONF_FILENAME = "src/main/resources/config.properties";
  public static final String CONS_INP_ERROR = "Error reading from console.";
  public static final String BAD_MENU_ITM = "Bad menu item";
  public static final String SEL_MENU_ITM = "Select menu item: ";
  public static final String ASK_RECORD_NUM = "Choose record number: ";
  public static final String MAINTAIN_CHOICE =
      "{A]dd record\n[C]hange record\n[D]elete record\n[E]exit\n: ";
  public static final String STMT_ERROR = "Cannot perform statement operation";
  public static final String PAUSE_MSG = "Press Enter to continue\n";
  public static final String NOT_REDIRECTED = "We can not redirect a parcel now\n";
  public static final String REDIRECTED = "Your parcel was redirected successfully\n";
  public static final String STAT_TAB_HEADER = "%30s%10s%n";
  public static final String NO_PARCEL = "No such parcel\n";
  public static final String PARCEL_CODE = "Choose parcel code: ";
  public static final String LEAVE = "0. Leave this menu";
  public static final String STAT_TAB_ROW = "%30s%10.2f%n";
  public static final String WRONG_PHONE = "Wrong phone number format.\n";
  public static final String ASK_PHONE = "Your mobile phone number: ";
  public static final String DB_CONN_ERR = "Error while connecting to the database\n";
  public static final String FILE_ERR = "File operation error\n";
  public static int HEIGHT = 10;
  public static final String DISTR_FORMAT = "Code: %d District: %s%n";
  public static final String ASK_NAME = "Enter the name: ";
  public static final String NO_DATA_SET = "Error while updating data, try next time.\n";
  public static final String NO_RECORD_FOUND = "No record found.\n";
  public static final String DELETE_SUCS = "Data successfully deleted.\n";
  public static final String SEPARATOR = "==================================\n";
  public static final String CITY_FORMAT = "Code: %d  City: %s Situated in: %s district%n";
  public static final String ASK_DISTR = "Enter district code: ";
  public static final String DEP_FORMAT = "Code: %d  Dept nr: %d%nCity: %s %s%n";
  public static final String ASK_D_NUMBER = "Enter department number: ";
  public static final String ASK_CITY = "Enter city code: ";
  public static final String ASK_ADRESS = "Enter department address : ";
  public static final String PRICE_FORMAT = "Category: %d  Price: %.2f grn/km%n";
  public static final String ASK_PRICE_CAT = "Enter price category: ";
  public static final String ASK_PRICE = "Enter price: ";
  public static final String DISTANCE_FORMAT = "Route %d%nFrom: %s  To: %s%nDistance: %d km%n";
  public static final String ASK_R_START = "Choose route start point: ";
  public static final String ASK_R_DESTIN = "Choose route destination point: ";
  public static final String ASK_DIST = "Set route distance: ";
  public static final String ASK_DEP = "Choose department closest to you: ";
  public static final String CLIENT_FORMAT = "Mr/Mrs %s %s%nMobile: %s%n";
  public static final String ASK_SURNAME = "Enter a surname: ";
  public static final String ASK_PASSPORT = "Enter your ID number: ";
  public static final String PARCEL_FORMAT = "Parcel no. %d%nFrom: %s To: %s%nArrive on: %te %3$B%n";
  public static final String ASK_WEIGHT = "Enter parcel weight: ";
  public static final String ASK_VOL = "Enter parcel vol: ";

  private TxtConsts() {
  }

}
package com.epam.mvnproj.bus.service;

import com.epam.mvnproj.model.Transit;
import com.epam.mvnproj.model.dao.DAO;
import com.epam.mvnproj.model.dao.TransitDao;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class TransitService {

  public List<Transit> findAll() throws SQLException {
    DAO<Transit, Integer> data = new TransitDao();
    List<Transit> output = data.getAll();
    data.closeConnection();
    return output;
  }

  public boolean addCity(Transit t) throws SQLException {
    DAO<Transit, Integer> data = new TransitDao();
    boolean output = data.create(t);
    data.closeConnection();
    return output;
  }

  public Transit fetchCityById(Integer id) throws SQLException {
    DAO<Transit, Integer> data = new TransitDao();
    Optional<Transit> output = data.getById(id);
    data.closeConnection();
    return output.orElseThrow(IllegalArgumentException::new);
  }

  public boolean changeCity(Transit t) throws SQLException {
    DAO<Transit, Integer> data = new TransitDao();
    boolean output = data.update(t);
    data.closeConnection();
    return output;
  }

  public boolean removeCity(Integer id) throws SQLException {
    DAO<Transit, Integer> data = new TransitDao();
    boolean output = data.delete(id);
    data.closeConnection();
    return output;
  }
}

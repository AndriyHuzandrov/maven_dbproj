package com.epam.mvnproj.bus.service;

import com.epam.mvnproj.model.City;
import com.epam.mvnproj.model.dao.CityDao;
import com.epam.mvnproj.model.dao.DAO;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class CityService implements Fetchable<City> {

  public List<City> findAll() throws SQLException {
    DAO<City, Integer> data = new CityDao();
    List<City> output = data.getAll();
    data.closeConnection();
    return output;
  }

  public boolean addCity(City d) throws SQLException {
    DAO<City, Integer> data = new CityDao();
    boolean output = data.create(d);
    data.closeConnection();
    return output;
  }

  public City fetchCityById(Integer id) throws SQLException {
    DAO<City, Integer> data = new CityDao();
    Optional<City> output = data.getById(id);
    data.closeConnection();
    return output.orElseThrow(IllegalArgumentException::new);
  }

  public boolean changeCity(City city) throws SQLException {
    DAO<City, Integer> data = new CityDao();
    boolean output = data.update(city);
    data.closeConnection();
    return output;
  }

  public boolean removeCity(Integer id) throws SQLException {
    DAO<City, Integer> data = new CityDao();
    boolean output = data.delete(id);
    data.closeConnection();
    return output;
  }
}

package com.epam.mvnproj.bus.service;

import com.epam.mvnproj.model.Price;
import com.epam.mvnproj.model.dao.DAO;
import com.epam.mvnproj.model.dao.PriceDao;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class PriceService implements Fetchable<Price> {

  public List<Price> findAll() throws SQLException {
    DAO<Price, Integer> data = new PriceDao();
    List<Price> output = data.getAll();
    data.closeConnection();
    return output;
  }

  public boolean addPrice(Price price) throws SQLException {
    DAO<Price, Integer> data = new PriceDao();
    boolean output = data.create(price);
    data.closeConnection();
    return output;
  }

  public Price fetchPriceByCat(Integer id) throws SQLException {
    DAO<Price, Integer> data = new PriceDao();
    Optional<Price> output = data.getById(id);
    data.closeConnection();
    return output.orElseThrow(IllegalArgumentException::new);
  }

  public boolean changePrice(Price price) throws SQLException {
    DAO<Price, Integer> data = new PriceDao();
    boolean output = data.update(price);
    data.closeConnection();
    return output;
  }

  public boolean removePrice(Integer id) throws SQLException {
    DAO<Price, Integer> data = new PriceDao();
    boolean output = data.delete(id);
    data.closeConnection();
    return output;
  }
}

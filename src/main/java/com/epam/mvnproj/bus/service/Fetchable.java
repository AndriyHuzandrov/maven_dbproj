package com.epam.mvnproj.bus.service;

import java.sql.SQLException;
import java.util.List;

public interface Fetchable<T> {
  List<T> findAll() throws SQLException;

}

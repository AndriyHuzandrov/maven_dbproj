package com.epam.mvnproj.bus.service;

import com.epam.mvnproj.model.District;
import com.epam.mvnproj.model.dao.DAO;
import com.epam.mvnproj.model.dao.DistrictDao;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class DistrictService implements Fetchable<District> {

  public List<District> findAll() throws SQLException {
    DAO<District, Integer> data = new DistrictDao();
    List<District> output = data.getAll();
    data.closeConnection();
    return output;
  }

  public boolean addDistrict(District d) throws SQLException {
    DAO<District, Integer> data = new DistrictDao();
    boolean output = data.create(d);
    data.closeConnection();
    return output;
  }

  public District fetchDistrictById(Integer id) throws SQLException {
    DAO<District, Integer> data = new DistrictDao();
    Optional<District> output = data.getById(id);
    data.closeConnection();
    return output.orElseThrow(IllegalArgumentException::new);
  }

  public boolean changeDistrict(District d) throws SQLException {
    DAO<District, Integer> data = new DistrictDao();
    boolean output = data.update(d);
    data.closeConnection();
    return output;
  }

  public boolean removeDistrict(Integer id) throws SQLException {
    DAO<District, Integer> data = new DistrictDao();
    boolean output = data.delete(id);
    data.closeConnection();
    return output;
  }
}

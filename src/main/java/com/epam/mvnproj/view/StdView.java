package com.epam.mvnproj.view;

import static com.epam.mvnproj.txtconst.TxtConsts.*;

public class StdView implements Displayable {

  public void display(String data) {
    appLog.info(data);
  }
}

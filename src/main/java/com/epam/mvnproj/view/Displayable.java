package com.epam.mvnproj.view;

public interface Displayable {
  void display(String data);
}

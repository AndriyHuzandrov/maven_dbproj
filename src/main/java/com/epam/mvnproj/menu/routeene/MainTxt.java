package com.epam.mvnproj.menu.routeene;

import java.util.LinkedList;
import java.util.List;

public class MainTxt extends Showable {

  List<String> fillMenu() {
    menu = new LinkedList<>();
    menu.add("1. Parcel processing.");
    menu.add("2. Parcel tracking.");
    menu.add("3. Maintainance.");
    menu.add("0. Exit program");
    return menu;
  }

}

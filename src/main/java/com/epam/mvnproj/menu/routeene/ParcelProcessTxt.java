package com.epam.mvnproj.menu.routeene;

import static com.epam.mvnproj.txtconst.TxtConsts.*;
import java.util.LinkedList;
import java.util.List;

public class ParcelProcessTxt extends Showable {

  List<String> fillMenu() {
    menu = new LinkedList<>();
    menu.add("1. Send parcel.");
    menu.add("2. Receive parcel.");
    menu.add(LEAVE);
    return menu;
  }
}

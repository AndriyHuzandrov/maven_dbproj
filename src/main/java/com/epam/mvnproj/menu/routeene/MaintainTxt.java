package com.epam.mvnproj.menu.routeene;

import static com.epam.mvnproj.txtconst.TxtConsts.*;
import java.util.LinkedList;
import java.util.List;

public class MaintainTxt extends Showable {

  List<String> fillMenu() {
    menu = new LinkedList<>();
    menu.add("1. District data.");
    menu.add("2. City data.");
    menu.add("3. Distance data.");
    menu.add("4. Department data.");
    menu.add("5. Prices.");
    menu.add(LEAVE);
    return menu;
  }
}

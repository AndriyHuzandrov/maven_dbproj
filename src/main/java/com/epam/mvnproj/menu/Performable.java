package com.epam.mvnproj.menu;

@FunctionalInterface
public interface Performable {
  void perform();
}

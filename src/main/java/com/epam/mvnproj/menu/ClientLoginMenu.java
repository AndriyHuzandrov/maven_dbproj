package com.epam.mvnproj.menu;

import static com.epam.mvnproj.txtconst.TxtConsts.CONS_INP_ERROR;
import static com.epam.mvnproj.txtconst.TxtConsts.ASK_PHONE;
import static com.epam.mvnproj.txtconst.TxtConsts.WRONG_PHONE;
import static com.epam.mvnproj.txtconst.TxtConsts.appLog;

import com.epam.mvnproj.UI.UI;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Optional;

public class ClientLoginMenu extends Menu {

  HashMap<String, Performable> prepareExeMenu() {
    HashMap<String, Performable> menu = new LinkedHashMap<>();
    menu.put("0", this::exit);
    menu.put("1", this::makeLoginProcess);
    return menu;
  }

  void exit() {
    Menu m = UI.newMainMenu();
    m.getChoice();
  }
  private void makeLoginProcess() {
    Optional<String> phone = Optional.empty();
    appLog.info(ASK_PHONE);
    try {
      phone = Optional.of(br.readLine().trim());
      if(!phone.get().matches("^0\\d{9}")) {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      appLog.error(CONS_INP_ERROR);
    } catch (IllegalArgumentException e) {
      appLog.warn(WRONG_PHONE);
      makeLoginProcess();
    }
    Menu m = UI.newParcTrackMenu(phone.orElseThrow(IllegalArgumentException::new));
    m.getChoice();
  }

}

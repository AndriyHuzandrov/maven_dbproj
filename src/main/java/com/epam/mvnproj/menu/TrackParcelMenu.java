package com.epam.mvnproj.menu;

import static com.epam.mvnproj.txtconst.TxtConsts.*;
import com.epam.mvnproj.UI.UI;
import com.epam.mvnproj.menu.routeene.RedirectTxt;
import com.epam.mvnproj.menu.routeene.Showable;
import com.epam.mvnproj.menu.routeene.SimpleExitTxt;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Optional;

public class TrackParcelMenu extends Menu {

  HashMap<String, Performable> prepareExeMenu() {
    HashMap<String, Performable> menu = new LinkedHashMap<>();
    menu.put("0", this::exit);
    menu.put("1", this::showParcels);
    return menu;
  }

  void exit() {
    Menu m = UI.newMainMenu();
    m.getChoice();
  }

  private String getParcelList() {
    return appControl.fetchAllParcels(getClient_id());
  }

  private void showParcels() {
    Showable parcelOperations;
    Optional<String> pCode = Optional.empty();
    monitor.display(getParcelList());
    monitor.display(PARCEL_CODE);
    try {
      pCode = Optional.of(br.readLine().trim());
    } catch (IOException e) {
      appLog.error(CONS_INP_ERROR);
    } catch (IllegalArgumentException e) {
      appLog.warn(NO_PARCEL);
      showParcels();
    }
    if(appControl.isClientSender(getClient_id(),
        pCode.orElseThrow(IllegalArgumentException::new))) {
      parcelOperations = new RedirectTxt();
    } else {
      parcelOperations = new SimpleExitTxt();
    }
    Menu m = UI.newRedirectMenu(parcelOperations,
        getClient_id(),
        pCode.orElseThrow(IllegalArgumentException::new));
    m.getChoice();
  }
}

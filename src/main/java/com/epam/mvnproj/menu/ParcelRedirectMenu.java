package com.epam.mvnproj.menu;

import static com.epam.mvnproj.txtconst.TxtConsts.*;
import com.epam.mvnproj.UI.UI;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class ParcelRedirectMenu extends Menu {

  HashMap<String, Performable> prepareExeMenu() {
    HashMap<String, Performable> menu = new LinkedHashMap<>();
    menu.put("0", this::exit);
    menu.put("1", this::redirect);

    return menu;
  }

  void exit() {
    Menu m = UI.newParcTrackMenu(getClient_id());
    m.getChoice();
  }

  private void redirect() {
    if(appControl.redirectParcel(getClient_id(), getParcel_id())) {
      appLog.info(REDIRECTED);
    } else {
      appLog.warn(NOT_REDIRECTED);
    }
    exit();
  }


}

package com.epam.mvnproj.model;

import static com.epam.mvnproj.txtconst.TxtConsts.*;

public class Distance {
  private Integer routeId;
  private Integer fromCity;
  private Integer toCity;
  private Integer distance;
  private String fromCityName;
  private String toCityName;

  public Integer getRouteId() {
    return routeId;
  }

  public void setRouteId(Integer routeId) {
    this.routeId = routeId;
  }

  public Integer getFromCity() {
    return fromCity;
  }

  public void setFromCity(Integer fromCity) {
    this.fromCity = fromCity;
  }

  public Integer getToCity() {
    return toCity;
  }

  public void setToCity(Integer toCity) {
    this.toCity = toCity;
  }

  public Integer getDistance() {
    return distance;
  }

  public void setDistance(Integer distance) {
    this.distance = distance;
  }

  public String getFromCityName() {
    return fromCityName;
  }

  public void setFromCityName(String fromCityName) {
    this.fromCityName = fromCityName;
  }

  public String getToCityName() {
    return toCityName;
  }

  public void setToCityName(String toCityName) {
    this.toCityName = toCityName;
  }

  public String toString() {
    return String.format(DISTANCE_FORMAT, getRouteId(),  getFromCityName(), getToCityName(), getDistance());
  }
}

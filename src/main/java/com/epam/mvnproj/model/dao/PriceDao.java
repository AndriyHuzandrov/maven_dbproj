package com.epam.mvnproj.model.dao;

import com.epam.mvnproj.model.Price;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PriceDao extends DAO<Price, Integer> {
  private static final String SELECT_ALL = "SELECT * FROM price";
  private static final String INSERT = "INSERT INTO price (cat, price) VALUES (?, ?)";
  private static final String CHOOSE = "SELECT cat, price FROM price WHERE cat = ?";
  private static final String UPDATE = "UPDATE price SET cat = ?, price = ? WHERE cat = ?";
  private static final String DELETE = "DELETE FROM price WHERE cat = ?";

  public List<Price> getAll() throws SQLException {
    Connection connection = makeDbConnection();
    List<Price> priceList = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL)){
      try(ResultSet results = statement.executeQuery()) {
        while(results.next()) {
          Price item = new Price();
          item.setCategory(results.getInt(1));
          item.setPrice(results.getDouble(2));
          priceList.add(item);
        }
      }
    }
    return priceList;
  }

  public boolean update(Price price) throws SQLException {
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
      statement.setInt(1, price.getCategory());
      statement.setDouble(2, price.getPrice());
      statement.setInt(3, price.getCategory());
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public Optional<Price> getById(Integer id) throws SQLException {
    Connection connection = makeDbConnection();
    Optional<Price> output = Optional.empty();
    try (PreparedStatement statement = connection.prepareStatement(CHOOSE)) {
      statement.setInt(1, id);
      try (ResultSet results = statement.executeQuery()) {
        while (results.next()) {
          Price item = new Price();
          item.setCategory(results.getInt(1));
          item.setPrice(results.getDouble(2));
          output = Optional.of(item);
        }
      }
    }
    return  output;
  }

  public boolean delete(Integer id) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(DELETE)){
      statement.setInt(1, id);
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public boolean create(Price price) throws SQLException {
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(INSERT)){
      statement.setInt(1, price.getCategory());
      statement.setDouble(2, price.getPrice());
      count = statement.executeUpdate();
    }
    return count > 0;
  }
}

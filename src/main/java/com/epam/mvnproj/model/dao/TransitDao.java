package com.epam.mvnproj.model.dao;

import com.epam.mvnproj.model.Transit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TransitDao extends DAO<Transit, Integer> {
  private static final String SELECT_ALL = "SELECT * FROM transit";
  private static final String INSERT = "INSERT INTO transit VALUES (?, ?, ?)";
  private static final String CHOOSE = "SELECT * FROM transit WHERE parcel = ?";
  private static final String UPDATE = "UPDATE transit SET to_dep = ?,  WHERE parcel = ?";
  private static final String DELETE = "DELETE FROM transit WHERE parcel = ?";

  public List<Transit> getAll() throws SQLException {
    Connection connection = makeDbConnection();
    List<Transit> transits = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL)){
      try(ResultSet results = statement.executeQuery()) {
        while(results.next()) {
          Transit item = new Transit();
          item.setParcelId(results.getInt(1));
          item.setDestinationDep(results.getInt(2));
          item.setRoute(results.getInt(3));
          transits.add(item);
        }
      }
    }
    return transits;
  }

  public boolean update(Transit t) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(UPDATE)){
      statement.setInt(1, t.getDestinationDep());
      statement.setInt(2, t.getParcelId());
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public Optional<Transit> getById(Integer id) throws SQLException {
    Connection connection = makeDbConnection();
    Optional<Transit> output = Optional.empty();
    try (PreparedStatement statement = connection.prepareStatement(CHOOSE)) {
      statement.setInt(1, id);
      try (ResultSet results = statement.executeQuery()) {
        while (results.next()) {
          Transit item = new Transit();
          item.setParcelId(results.getInt(1));
          item.setDestinationDep(results.getInt(2));
          item.setRoute(results.getInt(3));
          output = Optional.of(item);
        }
      }
    }
    return  output;
  }

  public boolean delete(Integer id) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(DELETE)){
      statement.setInt(1, id);
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public boolean create(Transit t) throws SQLException {
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(INSERT)){
      statement.setInt(1, t.getParcelId());
      statement.setInt(2, t.getDestinationDep());
      statement.setInt(3, t.getRoute());
      count = statement.executeUpdate();
    }
    return count > 0;
  }
}

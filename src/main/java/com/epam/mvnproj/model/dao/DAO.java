package com.epam.mvnproj.model.dao;

import static com.epam.mvnproj.txtconst.TxtConsts.*;
import com.epam.mvnproj.connect.Connectible;
import com.epam.mvnproj.connect.ConnectionDispatch;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

public abstract class DAO<T, K> {
  private Connectible conns;

  public DAO() {
    Properties projConfig = prepareProps();
    try {
      conns = ConnectionDispatch.create(
          projConfig.getProperty("url"),
          projConfig.getProperty("user"),
          projConfig.getProperty("paswd")
      );
    } catch (SQLException e) {
      appLog.error(DB_CONN_ERR);
      appLog.error(e.getMessage());
    }
  }
  private Properties prepareProps() {
    Properties props = new Properties();
    try(FileInputStream fr = new FileInputStream(CONF_FILENAME)) {
      props.load(fr);
    } catch (IOException e) {
      appLog.error(FILE_ERR);
    }
    return props;
  }

  Connection makeDbConnection() {
    return conns.getConnection();
  }

  void unsetDbStatement(PreparedStatement ps) {
    Optional<PreparedStatement> st = Optional.ofNullable(ps);
    try {
      st.orElseThrow(IllegalStateException::new).close();
    } catch (SQLException e) {
      appLog.error(e.getMessage());
    } catch (IllegalStateException e) {
      appLog.error(STMT_ERROR);
    }
  }
  public void closeConnection() {
    try {
      conns.shutdown();
    } catch (SQLException e) {
      appLog.error(e.getMessage());
    } catch (IllegalStateException e) {
      appLog.error(STMT_ERROR);
    }
  }
  public abstract List<T> getAll() throws SQLException;
  public abstract boolean update(T entity) throws SQLException;
  public abstract Optional<T> getById(K id) throws SQLException;
  public abstract boolean delete(K id) throws SQLException;
  public abstract boolean create(T entity) throws SQLException;

}

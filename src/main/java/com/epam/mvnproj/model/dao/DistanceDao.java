package com.epam.mvnproj.model.dao;

import com.epam.mvnproj.model.Distance;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DistanceDao extends DAO<Distance, Integer> {
  private static final String SELECT_ALL =
      "SELECT d.route, d.from_city, d.to_city, d.dist, c.city_name, cc.city_name FROM distance d" +
          " INNER JOIN city c ON d.from_city = c.id_city" +
          " INNER JOIN city cc on d.to_city = cc.id_city";
  private static final String INSERT =
      "INSERT INTO distance (from_city, to_city, dist) VALUES (?, ?, ?)";
  private static final String CHOOSE =
      "SELECT d.route, d.from_city, d.to_city, d.dist, c.city_name, cc.city_name FROM distance d" +
          " INNER JOIN city c ON d.from_city = c.id_city" +
          " INNER JOIN city cc on d.to_city = cc.id_city" +
      " WHERE d.route = ?";
  private static final String UPDATE =
      "UPDATE distance SET from_city = ?, to_city = ?, dist = ? WHERE route = ?";
  private static final String DELETE = "DELETE FROM distance WHERE route = ?";

  public List<Distance> getAll() throws SQLException {
    Connection connection = makeDbConnection();
    List<Distance> distances = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL)){
      try(ResultSet results = statement.executeQuery()) {
        while(results.next()) {
          Distance item = new Distance();
          item.setRouteId(results.getInt(1));
          item.setFromCity(results.getInt(2));
          item.setToCity(results.getInt(3));
          item.setDistance(results.getInt(4));
          item.setFromCityName(results.getString(5));
          item.setToCityName(results.getString(6));
          distances.add(item);
        }
      }
    }
    return distances;
  }

  public boolean update(Distance d) throws SQLException {
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
      statement.setInt(1, d.getFromCity());
      statement.setInt(2, d.getToCity());
      statement.setInt(3, d.getDistance());
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public Optional<Distance> getById(Integer id) throws SQLException {
    Connection connection = makeDbConnection();
    Optional<Distance> output = Optional.empty();
    try (PreparedStatement statement = connection.prepareStatement(CHOOSE)) {
      statement.setInt(1, id);
      try (ResultSet results = statement.executeQuery()) {
        while (results.next()) {
          Distance item = new Distance();
          item.setRouteId(results.getInt(1));
          item.setFromCity(results.getInt(2));
          item.setToCity(results.getInt(3));
          item.setDistance(results.getInt(4));
          item.setFromCityName(results.getString(5));
          item.setToCityName(results.getString(6));
          output = Optional.of(item);
        }
      }
    }
    return  output;
  }

  public boolean delete(Integer id) throws SQLException{
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(DELETE)){
      statement.setInt(1, id);
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public boolean create(Distance d) throws SQLException {
    Connection connection = makeDbConnection();
    int count;
    try (PreparedStatement statement = connection.prepareStatement(INSERT)){
      statement.setInt(1, d.getFromCity());
      statement.setInt(2, d.getToCity());
      statement.setInt(3, d.getDistance());
      count = statement.executeUpdate();
    }
    return count > 0;
  }

}

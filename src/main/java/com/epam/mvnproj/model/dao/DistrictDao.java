package com.epam.mvnproj.model.dao;

import com.epam.mvnproj.model.District;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DistrictDao extends DAO<District, Integer> {
  private static final String SELECT_ALL = "SELECT * FROM district";
  private static final String INSERT = "INSERT INTO district (distr_name) VALUES (?)";
  private static final String CHOOSE = "SELECT * FROM district WHERE distr_id = ?";
  private static final String UPDATE = "UPDATE district SET distr_name = ? WHERE distr_id = ?";
  private static final String DELETE = "DELETE FROM district WHERE distr_id = ?";

  public List<District> getAll() throws SQLException {
    Connection connection = makeDbConnection();
    List<District> districts = new ArrayList<>();
    try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL)){
      try(ResultSet results = statement.executeQuery()) {
        while(results.next()) {
          District item = new District();
          item.setDistId(results.getInt(1));
          item.setDistrName(results.getString(2));
          districts.add(item);
        }
      }
    }
    return districts;
  }
  public boolean update(District d) throws SQLException{
    Connection connection = makeDbConnection();
    int count = 0;
    try (PreparedStatement statement = connection.prepareStatement(UPDATE)){
      statement.setString(1, d.getDistrName());
      statement.setInt(2, d.getDistId());
      count = statement.executeUpdate();
    }
    return count > 0;
  }
  public Optional<District> getById(Integer id) throws SQLException {
    Connection connection = makeDbConnection();
    Optional<District> output = Optional.empty();
    try (PreparedStatement statement = connection.prepareStatement(CHOOSE)) {
      statement.setInt(1, id);
      try (ResultSet results = statement.executeQuery()) {
        while (results.next()) {
          District item = new District();
          item.setDistId(results.getInt(1));
          item.setDistrName(results.getString(2));
          output = Optional.of(item);
        }
      }
    }
    return  output;
  }

  public boolean delete(Integer id) throws SQLException{
    Connection connection = makeDbConnection();
    int count = 0;
    try (PreparedStatement statement = connection.prepareStatement(DELETE)){
      statement.setInt(1, id);
      count = statement.executeUpdate();
    }
    return count > 0;
  }

  public boolean create(District d) throws SQLException {
    Connection connection = makeDbConnection();
    int count = 0;
    try (PreparedStatement statement = connection.prepareStatement(INSERT)){
     statement.setString(1, d.getDistrName());
     count = statement.executeUpdate();
    }
    return count > 0;
  }
}

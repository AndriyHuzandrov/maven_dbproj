package com.epam.mvnproj.model;

import static com.epam.mvnproj.txtconst.TxtConsts.*;

public class Price {
  private Integer category;
  private Double price;

  public Integer getCategory() {
    return category;
  }

  public void setCategory(Integer category) {
    this.category = category;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String toString() {
    return String.format(PRICE_FORMAT, getCategory(), getPrice());
  }
}

package com.epam.mvnproj.model;

import static com.epam.mvnproj.txtconst.TxtConsts.*;

public class Department {
  private Integer depId;
  private Integer depNum;
  private Integer city;
  private String cityName;
  private String address;

  public Integer getDepId() {
    return depId;
  }

  public void setDepId(Integer depId) {
    this.depId = depId;
  }

  public Integer getDepNum() {
    return depNum;
  }

  public void setDepNum(Integer depNum) {
    this.depNum = depNum;
  }

  public Integer getCity() {
    return city;
  }

  public void setCity(Integer city) {
    this.city = city;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }
  public String toString() {
    return String.format(DEP_FORMAT, getDepId(), getDepNum(), getCityName(), getAddress());
  }
}

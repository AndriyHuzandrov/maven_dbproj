package com.epam.mvnproj.model;

public class Transit {
  private Integer parcelId;
  private Integer destinationDep;
  private Integer route;

  public Integer getParcelId() {
    return parcelId;
  }

  public void setParcelId(Integer parcelId) {
    this.parcelId = parcelId;
  }

  public Integer getDestinationDep() {
    return destinationDep;
  }

  public void setDestinationDep(Integer destinationDep) {
    this.destinationDep = destinationDep;
  }

  public Integer getRoute() {
    return route;
  }

  public void setRoute(Integer route) {
    this.route = route;
  }
}

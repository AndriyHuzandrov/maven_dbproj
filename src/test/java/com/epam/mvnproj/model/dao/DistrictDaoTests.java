package com.epam.mvnproj.model.dao;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import com.epam.mvnproj.model.District;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

//@RunWith(MockitoJUnitRunner.class)
public class DistrictDaoTests {
/*
  @InjectMocks
  DistrictDao data;
  @Mock
  Connection connection;
  @Mock
  PreparedStatement stmt;
  @BeforeAll
  public void setUp() throws SQLException {
    when(connection.prepareStatement(eq("INSERT INTO district (distr_name) VALUES (?)"))).thenReturn(stmt);
    when(stmt.executeUpdate()).thenReturn(1);
  }



  @Test
  public void testInsertNewDistrict() throws SQLException{
    Assertions.assertTrue(data.create(new District()));
  }
 */
  @Test
  @DisplayName("Check dao for getting all recors")
  void returnedListSizeSameAsDBTable() throws SQLException{
    DAO<District, Integer> data = new DistrictDao();
    Assertions.assertEquals(3, data.getAll().size());
    data.closeConnection();
  }

  @Test
  @DisplayName("Get record by ID")
  void getRecordWhenNameIsSameThenOK() throws SQLException {
    DAO<District, Integer> data = new DistrictDao();
    Optional<District> result = data.getById(2);
    Assertions.assertEquals("Vinitska",
        result.orElseGet(() -> new District()).getDistrName());
    data.closeConnection();
  }

  @Test
  @DisplayName("Check exception on duplocate insert")
  void whenDoubleInsertionThenSQLException() throws SQLException {
    DAO<District, Integer> data = new DistrictDao();
    District testDistr = new District();
    testDistr.setDistrName("Sumska");
    Assertions.assertThrows(SQLException.class,
        () -> data.create(testDistr));

  }

}

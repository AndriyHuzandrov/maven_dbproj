package com.epam.mvnproj.connect;

import static com.epam.mvnproj.txtconst.TxtConsts.CONF_FILENAME;
import static com.epam.mvnproj.txtconst.TxtConsts.FILE_ERR;
import static com.epam.mvnproj.txtconst.TxtConsts.appLog;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ConnPoolTests {
  static Properties props;

  @BeforeAll
  static void makeConString() {
    props = new Properties();
    try(FileInputStream fr = new FileInputStream(CONF_FILENAME)) {
      props.load(fr);
    } catch (IOException e) {
      appLog.error(FILE_ERR);
    }
  }

  @Test
  @DisplayName("Con exist test")
  void  whenExtractFromPoolAndIsAlive() throws SQLException {
    Connectible cPool = ConnectionDispatch.create(
        props.getProperty("url"),
        props.getProperty("user"),
        props.getProperty("paswd")
    );
    Assertions.assertTrue(cPool.getConnection().isValid(1),
        () -> "Error while pool creation");
    cPool.shutdown();
  }

  @Test
  @DisplayName("Pool closure check")
  void whenPoolClosedAndGetConnectionThenError() throws SQLException {
    Connectible cPool = ConnectionDispatch.create(
        props.getProperty("url"),
        props.getProperty("user"),
        props.getProperty("paswd")
    );
    cPool.shutdown();
    Assertions.assertThrows(Exception.class, () -> cPool.getConnection());
  }

}

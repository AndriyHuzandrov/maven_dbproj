package com.epam.mvnproj.menu;

import com.epam.mvnproj.UI.UI;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MainMenuTests {
  static Menu testMainMenu;

  @BeforeAll
  static void getSampleMenu() {
    testMainMenu = UI.newMainMenu();
  }

  @Test
  @DisplayName("Main menu build check")
  void whenMainMenuCreatedMapHasSize() {
    Map<String, Performable> newMenu = testMainMenu.prepareExeMenu();
    Assertions.assertEquals(4, newMenu.size(),
    () -> "Wrong exe menu item count");
  }

  @Test
  @DisplayName("Wrong menu choice exception check")
  void whenWrongInputMustThrowExcept() {

  }
}
